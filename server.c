#include "server.h"
#include  "message.h"

void server()
{
    /**
     * Create the message queue
     *
     * read any messages from the queue
     *
     * if there is a message from the queue, read it
     * Open the message
     *
     * if sucessful opening the message, write contents to message queue
     *
     */

    pid_t clientservproc;
    int bytesread;

    Mesg_t *message;
    message = (Mesg_t *) malloc(sizeof(Mesg_t));

    int msgq_id = open_queue();

    signal(SIGINT, handle_death);
    signal(SIGCHLD, handle_death);

    while (1)
    {
        message->mesg_type = 1;
        if ((bytesread = read_msgq(msgq_id, 1, message)) == -1 || errno == EINTR)
        {
            if (errno == EINTR)
            {
                errno = 0;
                continue;
            }

            fprintf(stderr, "failed reading from message queue!\n");
            continue;
        }

        if ((clientservproc = fork()) == 0) //service a client in a different process
        {
            serve_client(message);
            free(message);
            exit(0);
        }
        else //parent code, just keep going.
        {   
            continue;   
        }
    }
}

void serve_client(Mesg_t *message)
{
    FILE *file;

    char *path, *buffer;
    path = (char*) malloc(sizeof(char) * MAXMESGDATA);
    buffer = (char*) malloc(sizeof(char) * MAXMESGDATA);

    pid_t clientpid;
    int servicepriority;

    int msgq_id = getmsgq_id();


    strcpy(path, message->mesg_data);

    clientpid = message->pid;
    message->mesg_type = clientpid;

    //set priority
    servicepriority = message->priority;

    if (servicepriority == 1)
        setpriority(PRIO_PROCESS, getpid(), 19);
    else if (servicepriority == 3)
        setpriority(PRIO_PROCESS, getpid(), -20);
    else
        setpriority(PRIO_PROCESS, getpid(), 0);

    if ((file = fopen(path, "r")) == NULL)
    {
        snprintf(message->mesg_data, MAXMESGDATA, "Couldn't open the file: %s\nclosing client...\n", path);
        send_msg_to_msgq(msgq_id, message);
    }
    else
    {
        while (fgets(message->mesg_data, MAXMESGDATA, file) != NULL)
        {
            message->mesg_len = strlen(message->mesg_data);
            send_msg_to_msgq(msgq_id, message);
        }
        fclose(file);
    }

    //notify the client that the server has finished reading file by setting pid = 0
    message->pid = 0; 
    memset(message->mesg_data, '\0', MAXMESGDATA);
    send_msg_to_msgq(msgq_id, message);
    
    free(path);
}

void handle_death(int signo)
{
    if (signo == SIGINT)
    {
        int msgq_id = getmsgq_id();
        close_msgq(msgq_id);
        exit(0);
    }

    if (signo == SIGCHLD)
    {
        wait(0);
    }    
}

/*---------------------------------------------------------------------------------------------
-- FUNCTION: terminate
--
-- DATE: January 21, 2013
--
-- REVISIONS: (Date and Description)
--
-- DESIGNER: Ronald Bellido
--
-- PROGRAMMER: Ronald Bellido
--
-- INTERFACE: void terminate(pid_t chilpid)
--          childpid - the pid of the (child) process to kill
--
-- RETURNS: void
--
-- NOTES:
This function is a wrapper for the kill() function.
-------------------------------------------------------------------------------------------------*/
void terminate(pid_t childpid)
{
    if ((kill(childpid, SIGTERM)) == -1)
        fatal("error killing child process!\r\n");
}