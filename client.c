#include "client.h"

void client(int priority)
{
    pthread_t readqthread;

    Mesg_t *message;
    message = malloc(sizeof(Mesg_t));
    message->pid = getpid();
    message->priority = priority;

    char *filename;
    filename = message->mesg_data;

    int msgq_id = getmsgq_id();
    
    char *nline;

    //scan filename and remove newline character
    fprintf(stdout, "Enter filename: ");
    fgets(filename, MAXMESGDATA, stdin);
    if ((nline = strchr(filename, '\n')) != NULL)
        *nline = '\0';

    message->mesg_type = 1;
    if ((send_msg_to_msgq(msgq_id, message)) == -1)
        fatal("send_msg_to_msgq failed!\n");

    pthread_create(&readqthread, NULL, read_mqueue, (void*) message);
    pthread_join(readqthread, NULL);

    free(message);
}

void* read_mqueue(void *msg)
{
    int msgq_id = getmsgq_id();

    Mesg_t *message;
    message = (Mesg_t*) msg;

    message->mesg_type = getpid();
    while (read_msgq(msgq_id, getpid(), message) > 0)
    {
        fprintf(stdout, "%s", message->mesg_data);
        if (message->pid == 0)
            break;
    }
}
