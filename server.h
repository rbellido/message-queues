#ifndef SERVER_H
#define SERVER_H

#include "message.h"
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <errno.h>

#define ERRMSGLEN 1024
void server();
void terminate(pid_t childpid);
void handle_death(int signo);
void serve_client(Mesg_t *message);

#endif
