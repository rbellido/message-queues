#ifndef MESSAGE_H
#define MESSAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAXMESGDATA (4096 - 16)
#define MESGHDRSIZE (sizeof(Mesg) - MAXMESGDATA)
#define MSGQKEY 1337

typedef struct
{
    long mesg_type;
    int mesg_len;
    int priority; //1: low, 2: medium, 3: high
    pid_t pid;
    char mesg_data[MAXMESGDATA];
} Mesg_t;

int send_msg_to_msgq(int msgqid, Mesg_t * message);
int read_msgq(int msgqid, long type, Mesg_t * message);
int getmsgq_id();
int open_queue();
int close_msgq();
void fatal(char * err_msg);

#endif
