#include "client.h"

/**
 * Main entry point for client.c program
 */
int main(int argc, char **argv)
{
    if (argc != 2)
    {
    	fprintf(stderr, 
    		"Usage: %s [client priority: 1 low, 2 medium, 3 high]\n", 
    		argv[0]);
    	exit(1);
    }
    
    int priority = atoi(argv[1]);  
    client(priority);
    return 0;
}
