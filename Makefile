COMPILER = gcc
FLAGS = -W -Wall

CLIENTEXECUTABLE = client
SERVEREXECUTABLE = server

CLIENTSOURCE = message.c client.c client_main.c
SERVERSOURCE = message.c server.c server_main.c

CLIENTOBJECTS = message.o client.o client_main.o
SERVEROBJECTS = message.o server.o server_main.o

LIBS = -lpthread

all: clean client server

client: $(CLIENTOBJECTS)
	$(COMPILER) $(FLAGS) $(CLIENTOBJECTS) -o $(CLIENTEXECUTABLE) $(LIBS)

server: $(SERVEROBJECTS)
	$(COMPILER) $(FLAGS) $(SERVEROBJECTS) -o $(SERVEREXECUTABLE)

clean:
	rm -f $(CLIENTEXECUTABLE) $(SERVEREXECUTABLE) *.o

debug:
	$(COMPILER) -c -g client_main.c
	$(COMPILER) -c -g server_main.c
	$(COMPILER) -c -g message.c
	$(COMPILER) -c -g client.c
	$(COMPILER) $(FLAGS) -g $(CLIENTOBJECTS) -o $(CLIENTEXECUTABLE) $(LIBS)
	$(COMPILER) -c -g server.c
	$(COMPILER) $(FLAGS) -g $(SERVEROBJECTS) -o $(SERVEREXECUTABLE)

debugclient:
	$(COMPILER) -c -g client_main.c
	$(COMPILER) -c -g client.c
	$(COMPILER) $(FLAGS) -g $(CLIENTOBJECTS) -o $(CLIENTEXECUTABLE) $(LIBS)

debugserver:
	$(COMPILER) -c -g server_main.c
	$(COMPILER) -c -g server.c
	$(COMPILER) $(FLAGS) -g $(SERVEROBJECTS) -o $(SERVEREXECUTABLE)

client.o:
	$(COMPILER) -c client.c

client_main.o:
	$(COMPILER) -c client_main.c

server_main.o:
	$(COMPILER) -c server_main.c

server.o:
	$(COMPILER) -c server.c

message.o:
	$(COMPILER) -c message.c

runclient: #Note the default priority is 2
	./$(CLIENTEXECUTABLE) 2

runserver:
	./$(SERVEREXECUTABLE)
