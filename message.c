#include "message.h"

/**
 * sends a message to the message queue
 */
int send_msg_to_msgq(int msgqid, Mesg_t * message)
{
    int length = sizeof(Mesg_t) - sizeof(long);
    int result;
    if ((result = msgsnd(msgqid, message, length, 0)) == -1)
        return -1;
    return result;
}

/**
 * Reads a message from the message queue
 */
int read_msgq(int msgqid, long type, Mesg_t * message)
{
    int length = sizeof(Mesg_t) - sizeof(long);
    int result = -1;
    if ((result = msgrcv(msgqid, message, length, type, 0)) == -1)
        return -1;
    return result;
}

/**
 * Gets the common message queue id created by the server
 */
int getmsgq_id()
{
    int msgq_id;
    if ((msgq_id = msgget(MSGQKEY, 0)) < 0)
        fatal("msgget failed!");
    return msgq_id;
}

/**
 * Creates a new message queue
 * with permissions 0664
 */
int open_queue()
{
    int qid;
    if ((qid = msgget(MSGQKEY, 0664|IPC_CREAT)) == -1)
        fatal("error creating message queue!");
    return qid;
}

/**
 * Removes the common message queue
 */
int close_msgq(int qid)
{
    if (msgctl(qid, IPC_RMID, 0) == -1)
        return -1;
    return 0;
}

void fatal(char * err_msg)
{
    perror(err_msg);
    exit(1);
}