#ifndef CLIENT_H
#define CLIENT_H

#include "message.h"
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void client(int priority);
void* read_mqueue(void*);

#endif
